Cette application propose une version optimisée pour Android du service de cartographie allemand maps.metager.de.

Les cartes du projet Openstreetmap sur lesquelles elle repose sont les meilleures que nous ayons rencontrées, notamment pour la randonnée et le vélo. De plus, elles ont un design pratique. Contrairement à la plupart des autres fournisseurs, vous restez totalement anonyme lorsque vous utilisez maps.metager.de - la sphère privée de nos utilisateurs est inviolable.

Mises à jour quotidiennes des données cartographiques.

Nous n'enregistrons pas de données personnelles et les transmettons encore moins à des tiers. maps.metager.de est actuellement encore en construction.

Fonctionnalités :

* Recherche dans les données cartographiques de noms de lieux, de rues, de POI, etc.
* Un seul clic de souris suffit pour lancer une recherche MetaGer sur les objets trouvés directement à partir des cartes, afin d'afficher des informations de fond plus détaillées.
* Planificateur d'itinéraire pour les piétons, les cyclistes et les voitures ; si vous le souhaitez, vous pouvez également utiliser votre position actuelle.
* Navigation pas à pas jusqu'à la destination.

Fonctions prévues :

* Assistance vocale pendant la navigation.
* Amélioration de la recherche dans les données cartographiques.
* Options d'itinéraire (itinéraire le plus rapide / itinéraire le plus court...).
* Inclusion de données actuelles sur le trafic.
