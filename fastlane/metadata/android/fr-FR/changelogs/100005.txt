Mise à jour du SDK cible à la version 35
Ajout de la prise en charge de la mise en page des applications bord à bord
Ajout de traductions pour les langues supportées
Mise à jour de Geckoview
Correction : Vérifier la permission d'installer des paquets avant la mise à jour de l'application (installation manuelle uniquement)
