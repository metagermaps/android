Update Target SDK to 35
Add support for edge-to-edge app layout
Add translations for supported languages
Update Geckoview
Fix: Check permission for installing packages before app update (manual installation only)