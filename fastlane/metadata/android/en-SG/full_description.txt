This app offers an Android-optimized version of the German map service maps.metager.de.

The underlying maps from the Openstreetmap project are the best we have come across, especially for hiking and cycling. And in a practical design. Unlike most other providers, you remain completely anonymous when using maps.metager.de - the privacy of our users is inviolable.

Daily updates of the map data.

We do not store any personal data and certainly do not pass it on to third parties. maps.metager.de is currently still under construction.

Functions:

* Search in the map data for place names, streets, POI's and much more.
* A MetaGer search for found objects can be started directly from the maps with a single mouse click in order to display more detailed background information.
* Route planner for pedestrians, cyclists and cars; if desired also using the current location.
* Step-by-step navigation to the destination.

Planned functions:

* Voice output during navigation.
* Improved search in the map data
* Route options (fastest route / shortest route...).
* Inclusion of current traffic data.
