Den här appen erbjuder en Android-optimerad version av den tyska karttjänsten maps.metager.de.

De underliggande kartorna från Openstreetmap-projektet är de bästa vi har stött på, särskilt för vandring och cykling. Och i en praktisk design. Till skillnad från de flesta andra leverantörer förblir du helt anonym när du använder maps.metager.de - våra användares integritet är okränkbar.

Dagliga uppdateringar av kartdata.

Vi lagrar inga personuppgifter och vidarebefordrar dem absolut inte till tredje part. maps.metager.de är för närvarande fortfarande under uppbyggnad.

Funktioner:

* Sök i kartdata efter platsnamn, gator, POI:er och mycket mer.
* En MetaGer-sökning efter hittade objekt kan startas direkt från kartorna med ett enda musklick för att visa mer detaljerad bakgrundsinformation.
* Ruttplanerare för fotgängare, cyklister och bilar; om så önskas även med hjälp av den aktuella platsen.
* Steg-för-steg-navigering till destinationen.

Planerade funktioner:

* Röststyrning under navigering.
* Förbättrad sökning i kartdata
* Ruttalternativ (snabbaste vägen / kortaste vägen ...).
* Inkludering av aktuella trafikdata.
