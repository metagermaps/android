Aggiornamento del Target SDK alla versione 35
Aggiunta del supporto per il layout delle app edge-to-edge
Aggiunta di traduzioni per le lingue supportate
Aggiornamento di Geckoview
Correzione: verifica dei permessi per l'installazione dei pacchetti prima dell'aggiornamento dell'app (solo installazione manuale)
