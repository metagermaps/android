Ta aplikacja oferuje zoptymalizowaną pod kątem systemu Android wersję niemieckiego serwisu mapowego maps.metager.de.

Mapy bazowe z projektu Openstreetmap są najlepsze, z jakimi mieliśmy do czynienia, szczególnie w przypadku turystyki pieszej i rowerowej. I to w praktycznym wydaniu. W przeciwieństwie do większości innych dostawców, pozostajesz całkowicie anonimowy podczas korzystania z maps.metager.de - prywatność naszych użytkowników jest nienaruszalna.

Codzienne aktualizacje danych map.

Nie przechowujemy żadnych danych osobowych i z pewnością nie przekazujemy ich stronom trzecim. maps.metager.de jest obecnie nadal w budowie.

Funkcje:

* Wyszukiwanie w danych mapy nazw miejsc, ulic, punktów POI i wielu innych.
* Wyszukiwanie MetaGer dla znalezionych obiektów można uruchomić bezpośrednio z map za pomocą jednego kliknięcia myszą, aby wyświetlić bardziej szczegółowe informacje podstawowe.
* Planowanie trasy dla pieszych, rowerzystów i samochodów; w razie potrzeby również z pomocą bieżącej lokalizacji.
* Nawigacja krok po kroku do celu.

Planowane funkcje:

* Komunikaty głosowe podczas nawigacji.
* Ulepszone wyszukiwanie w danych mapy
* Opcje trasy (najszybsza trasa / najkrótsza trasa...).
* Uwzględnienie aktualnych danych o ruchu drogowym.
