package de.metager.maps;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.metager.maps.util.androidUtil.PropertyLoader;

public class GeoURLActivity extends AppCompatActivity {
    @Override
    protected void onStart(){
        super.onStart();
        Uri data = getIntent().getData();
        if(data == null){
            Log.d("DEBUG", "Intent called without any data");
            finish();
            return;
        }

        // Intent data as described here: https://developer.android.com/guide/components/intents-common#ViewMap
        Pattern parsePattern = Pattern.compile("^geo:0,0\\?q=(.*)$");
        Matcher parseMatcher = parsePattern.matcher(data.toString());
        if(parseMatcher.matches()){
            String query = parseMatcher.group(1);
            String url = null;
            try {
                url = PropertyLoader.getProperty("maps_webserver_url", this);
            } catch (IOException e) {
                Log.d("DEBUG", "IO Exception while loading property");
            }

            url += "/search/" + query;

            Intent i = new Intent(this, MainActivity.class);
            i.putExtra("url", url);
            // Do not start another instance of MainActivity because there can only be one geckoview runtime
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
            this.finish();
        }
    }
}
