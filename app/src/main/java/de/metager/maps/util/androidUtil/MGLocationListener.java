package de.metager.maps.util.androidUtil;

import android.location.Location;

import androidx.annotation.Nullable;

public interface MGLocationListener extends android.location.LocationListener {
    void onLocationChanged(@Nullable Location location);
}
