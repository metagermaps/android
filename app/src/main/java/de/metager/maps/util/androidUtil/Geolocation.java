package de.metager.maps.util.androidUtil;

import android.location.Location;
import android.location.LocationManager;
import android.location.LocationRequest;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.RequiresPermission;

import org.json.JSONException;
import org.json.JSONObject;
import org.mozilla.geckoview.WebExtension;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class Geolocation {
    public final WebExtension.Port port;
    private final LocationManager locationManager;
    public final int requestID;
    public GeolocationOptions options;
    public android.location.LocationListener locationListener;


    public Geolocation(LocationManager locationManager, JSONObject options, WebExtension.Port port){
        this.locationManager = locationManager;
        this.options = new GeolocationOptions(options);
        int requestID;
        try {
            requestID = options.getInt("request_id");
        } catch (JSONException e) {
            requestID = Integer.MAX_VALUE;
            Log.e("MetaGerMaps", "Cannot find request_id for Location Request");
        }
        this.requestID = requestID;
        this.port = port;
    }

    /**
     * Implementation of navigator.geolocation.getCurrentPosition for our Android App
     * since Geolocation in Gecko is not using GPS correctly
     */
    @RequiresPermission(anyOf = {"android.permission.ACCESS_COARSE_LOCATION","android.permission.ACCESS_FINE_LOCATION"})
    @RequiresApi(Build.VERSION_CODES.S)
    public void getCurrentPosition(Executor executor, Consumer<Location> callback){
        final CancellationSignal cancellationSignal = new CancellationSignal();
        // Parse the Options for this location request
        List<String> availableProviders = locationManager.getAllProviders();
        LocationRequest.Builder locationRequestBuilder = new LocationRequest.Builder(options.maximumAge)
                .setMaxUpdateDelayMillis(50)
                .setDurationMillis(options.timeout)
                .setQuality(options.enableHighAccuracy ? LocationRequest.QUALITY_HIGH_ACCURACY : LocationRequest.QUALITY_BALANCED_POWER_ACCURACY);
        if(availableProviders.contains(LocationManager.FUSED_PROVIDER) && locationManager.isProviderEnabled(LocationManager.FUSED_PROVIDER))
        {
            // Easiest possibility: FUSED Provider is available
            locationManager.getCurrentLocation(LocationManager.FUSED_PROVIDER, locationRequestBuilder.build(), cancellationSignal, executor, callback);
        }else if((options.enableHighAccuracy || !availableProviders.contains(LocationManager.NETWORK_PROVIDER)) && availableProviders.contains(LocationManager.GPS_PROVIDER) && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            // GPS is available try to get a GPS fix first
            locationRequestBuilder.setDurationMillis(Math.min(2500, options.timeout / 2)); // Only wait max 2.5 seconds for a GPS fix
            locationManager.getCurrentLocation(LocationManager.GPS_PROVIDER, locationRequestBuilder.build(), cancellationSignal, executor, location -> {
                if(location == null && availableProviders.contains(LocationManager.NETWORK_PROVIDER) && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
                    // GPS fix was not acquired on time; Use network location instead
                    cancellationSignal.cancel();
                    locationRequestBuilder.setDurationMillis(options.timeout);
                    locationManager.getCurrentLocation(LocationManager.NETWORK_PROVIDER, locationRequestBuilder.build(), null, executor, callback);
                }else{
                    // Network Location not available or we already have a Location
                    callback.accept(location);
                }
            });
        }else if(availableProviders.contains(LocationManager.NETWORK_PROVIDER) && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationManager.getCurrentLocation(LocationManager.NETWORK_PROVIDER, locationRequestBuilder.build(), null, executor, callback);
        }else{
            // No Location provider available
            callback.accept(null);
        }
    }

    @RequiresPermission(anyOf = {"android.permission.ACCESS_COARSE_LOCATION","android.permission.ACCESS_FINE_LOCATION"})
    @Deprecated
    public void getCurrentPosition(Looper looper, MGLocationListener listener) {
        List<String> availableProviders = locationManager.getAllProviders();
        AtomicBoolean locationReceived = new AtomicBoolean(false);
        final Handler timeoutHandler = new Handler(looper);
        Runnable queuedTimeout = () -> {
            if (locationReceived.get()) return;
            Location location = null;
            listener.onLocationChanged(location);
        };
        MGLocationListener nestedListener = new MGLocationListener() {
            @Override
            public void onLocationChanged(@Nullable Location location) {
                locationReceived.set(true);
                timeoutHandler.removeCallbacks(queuedTimeout);
                listener.onLocationChanged(location);
            }
            @Override
            public void onProviderEnabled(@NonNull String provider) {

            }

            @Override
            public void onProviderDisabled(@NonNull String provider) {

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }
        };
        if((options.enableHighAccuracy || !availableProviders.contains(LocationManager.NETWORK_PROVIDER)) && availableProviders.contains(LocationManager.GPS_PROVIDER) && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, nestedListener, looper);
            timeoutHandler.postDelayed(() -> {
                if (locationReceived.get()) return;
                locationManager.removeUpdates(nestedListener);
                if (availableProviders.contains(LocationManager.NETWORK_PROVIDER) && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, nestedListener, looper);
                    timeoutHandler.postDelayed(queuedTimeout , options.timeout);
                }else{
                    Location location = null;
                    listener.onLocationChanged(location); // No location provider available
                }
            }, Math.min(2500, options.timeout / 2));
        }else if(availableProviders.contains(LocationManager.NETWORK_PROVIDER) && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, nestedListener, looper);
            timeoutHandler.postDelayed(queuedTimeout, options.timeout);
        }else{
            // No Location provider available
            Location location = null;
            listener.onLocationChanged(location);
        }
    }

    /**
     * Implementation of navigator.geolocation.getCurrentPosition for our Android App
     * since Geolocation in Gecko is not using GPS correctly
     */
    @RequiresPermission(anyOf = {"android.permission.ACCESS_COARSE_LOCATION","android.permission.ACCESS_FINE_LOCATION"})
    @RequiresApi(Build.VERSION_CODES.S)
    public void watchPosition(Executor executor, MGLocationListener callback){
        List<String> availableProviders = locationManager.getAllProviders();

        // Parse the Options for this location request

        LocationRequest.Builder locationRequestBuilder = new LocationRequest.Builder(1000)
                .setDurationMillis(options.timeout)
                .setQuality(options.enableHighAccuracy ? LocationRequest.QUALITY_HIGH_ACCURACY : LocationRequest.QUALITY_BALANCED_POWER_ACCURACY);
        if(options.maximumAge > 0){
            locationRequestBuilder.setIntervalMillis(options.maximumAge);
            locationRequestBuilder.setMinUpdateIntervalMillis(options.maximumAge);
        }
        this.locationListener = callback;
        if(availableProviders.contains(LocationManager.FUSED_PROVIDER) && locationManager.isProviderEnabled(LocationManager.FUSED_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.FUSED_PROVIDER, locationRequestBuilder.build(), executor, callback);
        }else if((options.enableHighAccuracy || !availableProviders.contains(LocationManager.NETWORK_PROVIDER)) && availableProviders.contains(LocationManager.GPS_PROVIDER) && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, locationRequestBuilder.build(), executor, callback);
        }else if(availableProviders.contains(LocationManager.NETWORK_PROVIDER) && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, locationRequestBuilder.build(), executor, callback);
        }else{
            Location nullLocation = null;
            callback.onLocationChanged(nullLocation);
            this.locationListener = null;
        }
    }

    @RequiresPermission(anyOf = {"android.permission.ACCESS_COARSE_LOCATION","android.permission.ACCESS_FINE_LOCATION"})
    @Deprecated
    public void watchPosition(Looper looper, MGLocationListener callback){
        List<String> availableProviders = locationManager.getAllProviders();

        // Accept Network location if last GPS position is less recent
        final long acceptNetworkLocationAfterMs = 2500;
        final long[] lastGPSPosition = new long[1];
        lastGPSPosition[0] = System.currentTimeMillis() - acceptNetworkLocationAfterMs;
        boolean gpsUpdatesEnabled = false;
        boolean networkUpdatesEnabled = false;

        MGLocationListener wrappedListener = new MGLocationListener() {
            @Override
            public void onLocationChanged(@Nullable Location location) {
                if(location == null) {
                    callback.onLocationChanged(location);
                    return;
                }
                if(Objects.equals(location.getProvider(), LocationManager.GPS_PROVIDER)){
                    lastGPSPosition[0] = System.currentTimeMillis();
                    callback.onLocationChanged(location);
                }else {
                    // Network Location Update
                    if(System.currentTimeMillis() - lastGPSPosition[0] > acceptNetworkLocationAfterMs) {
                        callback.onLocationChanged(location);
                    }
                }
            }

            @Override
            public void onProviderEnabled(@NonNull String provider) {

            }

            @Override
            public void onProviderDisabled(@NonNull String provider) {

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }
        };

        // Parse the Options for this location request
        long maxAge = 500;
        if(options.maximumAge > 0){
            maxAge = options.maximumAge;
        }

        this.locationListener = wrappedListener;
        if((options.enableHighAccuracy || !availableProviders.contains(LocationManager.NETWORK_PROVIDER)) && availableProviders.contains(LocationManager.GPS_PROVIDER) && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, maxAge, 0, wrappedListener, looper);
            gpsUpdatesEnabled = true;
        }
        if(availableProviders.contains(LocationManager.NETWORK_PROVIDER) && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, maxAge, 0, wrappedListener, looper);
            networkUpdatesEnabled = true;
        }
        if(!gpsUpdatesEnabled && ! networkUpdatesEnabled) {
            Location nullLocation = null;
            callback.onLocationChanged(nullLocation);
            this.locationListener = null;
        }
    }

    @RequiresApi(Build.VERSION_CODES.S)
    public void clearWatch(){
        if(this.locationListener != null){
            locationManager.removeUpdates(this.locationListener);
            this.locationListener = null;
        }
    }
    public void getCurrentPositionLegacy(){
        throw new Error("Not implemented yet");
    }
}


