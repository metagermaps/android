package de.metager.maps.util.androidUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class GeolocationOptions {
    public final long maximumAge;
    public final long timeout;
    public final boolean enableHighAccuracy;

    public GeolocationOptions(JSONObject json_options){
        long maximumAge;
        try {
            maximumAge = json_options.getLong("maximumAge");
        } catch (JSONException e) {
            // Field does not exist: Use default value
            maximumAge = 0;
        }
        this.maximumAge = Math.max(maximumAge, 50);

        long timeout;
        try {
            timeout = json_options.getLong("timeout");
            if(timeout <= 0){
                timeout = Long.MAX_VALUE;
            }
        } catch (JSONException e) {
            // Field does not exist: Use default value
            timeout = Long.MAX_VALUE;
        }
        this.timeout = timeout;

        boolean enableHighAccuracy;
        try {
            enableHighAccuracy = json_options.getBoolean("enableHighAccuracy");
        } catch (JSONException e) {
            // Field does not exist: Use default value
            enableHighAccuracy = false;
        }
        this.enableHighAccuracy = enableHighAccuracy;
    }
}
