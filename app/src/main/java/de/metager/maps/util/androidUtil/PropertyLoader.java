package de.metager.maps.util.androidUtil;

import android.content.Context;

import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {
    private static final String propertyFilepath = "app.properties";

    public static String getProperty(String key, Context context) throws IOException {
        Properties properties = new Properties();
        properties.load(context.getAssets().open(propertyFilepath));
        return properties.getProperty(key);
    }
}
