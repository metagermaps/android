package de.metager.maps.util.androidUtil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.annotation.Nullable;

public class ConnectivityUtil {
    public static boolean internetIsAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        @Nullable
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            return activeNetwork.isConnected();
        } else {
            return false;
        }
    }

    public static boolean isWireless(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        @Nullable
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (wifiInfo != null) {
            boolean isConnected = wifiInfo.getState() == NetworkInfo.State.CONNECTED;
            boolean isConnecting = wifiInfo.getState() == NetworkInfo.State.CONNECTING;
            return isConnected || isConnecting;
        } else {
            return false;
        }
    }
}
