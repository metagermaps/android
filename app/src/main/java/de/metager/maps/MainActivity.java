package de.metager.maps;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.google.common.util.concurrent.ListenableFuture;

import org.mozilla.gecko.util.ThreadUtils;
import org.mozilla.geckoview.BuildConfig;
import org.mozilla.geckoview.GeckoRuntime;
import org.mozilla.geckoview.GeckoRuntimeSettings;
import org.mozilla.geckoview.GeckoSession;
import org.mozilla.geckoview.GeckoView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.metager.maps.notifications.NotificationManager;
import de.metager.maps.permissions.PermissionManager;
import de.metager.maps.updater.UpdateChecker;
import de.metager.maps.util.androidUtil.PropertyLoader;
import de.metager.maps.webview.InsetChangeController;
import de.metager.maps.webview.MapsMessageDelegate;
import de.metager.maps.webview.MapsNavigationDelegate;
import de.metager.maps.webview.MapsPermissionDelegate;
import de.metager.maps.webview.MapsPortDelegate;
import de.metager.maps.webview.MapsPromptDelegate;

public class MainActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback {
    protected GeckoView webview;
    protected GeckoSession session;
    protected GeckoRuntime runtime;
    protected MapsPermissionDelegate permissionHandler;
    protected MapsMessageDelegate messageDelegate;
    protected MapsPortDelegate portDelegate;
    protected MapsNavigationDelegate navigationDelegate;
    protected MapsPromptDelegate promptDelegate;
    protected InsetChangeController insetChangeCOntroller;

    public static int LOCATION_PERMISSION_REQUEST_CODE = 0;
    private ArrayList<URL> allowedHosts;
    private URL loadURL;


    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        EdgeToEdge.enable(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        webview = findViewById(R.id.geckoview);
        this.session = new GeckoSession();
        GeckoRuntimeSettings settings = new GeckoRuntimeSettings.Builder().consoleOutput(true).build();
        settings.setConsoleOutputEnabled(true);
        if (BuildConfig.DEBUG) {
            settings.setRemoteDebuggingEnabled(true);
            settings.setAboutConfigEnabled(true);
        }

        if (this.runtime == null) {
            this.runtime = GeckoRuntime.create(this, settings);
        }

        this.loadURL = this.getWebserverURL();
        this.allowedHosts = new ArrayList<>(Arrays.asList(loadURL));

        if(BuildConfig.DEBUG) {
            // Allow Github Devtunnels in Debug ModeL(
            try {
                this.allowedHosts.add(new URL("https://visualstudio.com"));
                this.allowedHosts.add(new URL("https://github.com"));
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        }

        session.open(runtime);
        webview.setSession(session);
        // Setup Messaging between App and Webpage
        portDelegate = new MapsPortDelegate(this);
        messageDelegate = new MapsMessageDelegate(portDelegate);

        // History Delegate
        navigationDelegate = new MapsNavigationDelegate(allowedHosts, this);
        session.setNavigationDelegate(navigationDelegate);

        // Prompt Delegate
        promptDelegate = new MapsPromptDelegate(this);
        session.setPromptDelegate(promptDelegate);

        runtime.getWebExtensionController().ensureBuiltIn("resource://android/assets/messaging/", "messaging@suma-ev.de").accept(
                // Set delegate that will receive messages coming from this extension.
                extension -> ThreadUtils.runOnUiThread(
                        new Runnable() {
                            @Override
                            public void run() {
                                if (extension == null) return;

                                session.getWebExtensionController()
                                        .setMessageDelegate(extension, messageDelegate, "browser");
                            }
                        }),
                // Something bad happened, let's log an error
                e -> Log.e("MessageDelegate", "Error registering extension", e)
        );

        permissionHandler = new MapsPermissionDelegate(this);
        session.setPermissionDelegate(permissionHandler);

        if (savedInstanceState == null) {
            this.loadView();
        }
    }

    private void loadView() {
        String intentUriString = getIntent().getStringExtra("url");
        if (intentUriString != null && allowedHosts.contains(Uri.parse(intentUriString).getHost())) {
            session.loadUri(intentUriString);
        } else {
            session.loadUri(loadURL.toString());
        }

        // For manual Installation only: Check for updates
        if (de.metager.maps.BuildConfig.FLAVOR.equals("manual")) {
            this.createUpdateChecker();
        }
    }

    private URL getWebserverURL() {
        String url = null;
        try {
            url = PropertyLoader.getProperty("maps_webserver_url", this);
            return new URL(url);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void createUpdateChecker() {
        if (PermissionManager.checkNotificationPermission(this, true)) {
            NotificationManager.createUpdateNotificationChannel(this.getApplicationContext());
            // Periodically run Updatecheck and notify preferred in unmetered Networks
            Constraints constraints = new Constraints.Builder().setRequiredNetworkType(NetworkType.UNMETERED).build();
            PeriodicWorkRequest workRequest = new PeriodicWorkRequest.Builder(UpdateChecker.class, 1, TimeUnit.DAYS)
                    .addTag(UpdateChecker.PERIODIC_TAG)
                    .setConstraints(constraints).build();
            WorkManager.getInstance(this).enqueueUniquePeriodicWork(UpdateChecker.NAME, ExistingPeriodicWorkPolicy.CANCEL_AND_REENQUEUE, workRequest);
        }
    }

    /**
     * Called when the Android App is finally connected
     */
    public void androidAppConnected() {
        // Setup Watch for Inset changes
        insetChangeCOntroller = new InsetChangeController(findViewById(android.R.id.content), getResources().getDisplayMetrics().density, messageDelegate);
    }

    @Override
    public void onRequestPermissionsResult(int i, @NonNull String[] strings, @NonNull int[] ints) {
        super.onRequestPermissionsResult(i, strings, ints);
        if (i == MainActivity.LOCATION_PERMISSION_REQUEST_CODE) {
            this.permissionHandler.onRequestPermissionsResult(i, strings, ints);
        } else if (i == PermissionManager.SINGLE_LOCATION_REQUEST_JAVASCRIPT) {
            boolean granted = false;
            for (int permissionResult : ints) {
                if (permissionResult == PackageManager.PERMISSION_GRANTED) {
                    granted = true;
                    break;
                }
            }
            if (granted) {
                portDelegate.getCurrentPositions();
            } else {
                portDelegate.getCurrentPositionsError();
            }
        } else if (i == PermissionManager.WATCH_LOCATION_REQUEST_JAVASCRIPT) {
            boolean granted = false;
            for (int permissionResult : ints) {
                if (permissionResult == PackageManager.PERMISSION_GRANTED) {
                    granted = true;
                    break;
                }
            }
            if (granted) {
                portDelegate.watchPositions();
            } else {
                portDelegate.watchPositionsError();
            }
        } else if (i == PermissionManager.PERMISSION_POST_NOTIFICATIONS) {
            boolean granted = false;
            for (int permissionResult : ints) {
                if (permissionResult == PackageManager.PERMISSION_GRANTED) {
                    granted = true;
                    break;
                }
            }
            if (granted && de.metager.maps.BuildConfig.FLAVOR.equals("manual")) {
                this.createUpdateChecker();
            }
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        this.loadView();
        super.onNewIntent(intent);
    }

    @Override
    public void onBackPressed() {
        if (navigationDelegate.canGoBack) {
            session.goBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        session.close();
        runtime.shutdown();
        super.onDestroy();
    }

    boolean setIntentExtras(Intent intent, Uri data) {
        return false;
    }
}
