package de.metager.maps.permissions;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionManager {
    public static int SINGLE_LOCATION_REQUEST_JAVASCRIPT = 1;
    public static int WATCH_LOCATION_REQUEST_JAVASCRIPT = 2;
    public static int PERMISSION_POST_NOTIFICATIONS = 3;
    public static int PERMISSION_STORAGE = 4;

    public static String[] locationPermissionsToRequest = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    public static boolean checkLocationPermission(Activity activity, int requestCode){
        boolean permission_granted = PermissionManager.hasLocationPermission(activity);

        if(!permission_granted){
            activity.requestPermissions(locationPermissionsToRequest, requestCode);
        }
        return permission_granted;
    }

    public static boolean hasLocationPermission(Activity activity){
        // Check if the user already granted us one of the permissions
        for(String permission : locationPermissionsToRequest){
            if(ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED){
                return true;
            }
        }
        return false;
    }

    public static boolean checkNotificationPermission(AppCompatActivity activity, boolean showRationale){
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.TIRAMISU) {
            return true;
        }
        String notificationPermission = Manifest.permission.POST_NOTIFICATIONS;
        if(ContextCompat.checkSelfPermission(activity, notificationPermission) == PackageManager.PERMISSION_GRANTED){
            return true;
        }else if(showRationale && ActivityCompat.shouldShowRequestPermissionRationale(activity, notificationPermission)){
            new NotificationPermissionRationale().show(activity.getSupportFragmentManager(), NotificationPermissionRationale.TAG);
            return false;
        }
        activity.requestPermissions(new String[]{notificationPermission}, PermissionManager.PERMISSION_POST_NOTIFICATIONS);
        return false;
    }

    public static boolean checkStoragePermission(AppCompatActivity activity) {
        String storagePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        if(ContextCompat.checkSelfPermission(activity, storagePermission) == PackageManager.PERMISSION_GRANTED){
            return true;
        }
        activity.requestPermissions(new String[]{storagePermission}, PermissionManager.PERMISSION_STORAGE);
        return false;
    }
}
