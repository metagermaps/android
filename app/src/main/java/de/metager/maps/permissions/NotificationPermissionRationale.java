package de.metager.maps.permissions;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import de.metager.maps.R;

public class NotificationPermissionRationale extends DialogFragment {
    public static String TAG = "NotificationPermissionDialog";

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new AlertDialog.Builder(requireContext())
                .setMessage(getString(R.string.permissions_notification_message))
                .setPositiveButton(getString(R.string.permissions_allow), (dialog, which) -> {
                    PermissionManager.checkNotificationPermission((AppCompatActivity) requireActivity(), false);
                } )
                .create();
    }

}