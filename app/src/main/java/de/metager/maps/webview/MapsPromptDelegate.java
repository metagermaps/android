package de.metager.maps.webview;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import org.mozilla.geckoview.GeckoResult;
import org.mozilla.geckoview.GeckoSession;

import java.util.ArrayList;

public class MapsPromptDelegate implements GeckoSession.PromptDelegate {

    private final Activity mActivity;

    public MapsPromptDelegate(final Activity activity){
        mActivity = activity;
    }
    @Override
    public GeckoResult<PromptResponse> onChoicePrompt(
            final GeckoSession session, final ChoicePrompt prompt) {
        final GeckoResult<PromptResponse> res = new GeckoResult<PromptResponse>();
        onChoicePromptImpl(
                session, prompt.title, prompt.message, prompt.type, prompt.choices, prompt, res);
        return res;
    }

    private void onChoicePromptImpl(GeckoSession session, String title, String message, int type, ChoicePrompt.Choice[] choices, ChoicePrompt prompt, GeckoResult<PromptResponse> res) {
        final Activity activity = mActivity;
        if (activity == null) {
            res.complete(prompt.dismiss());
            return;
        }
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        addStandardLayout(builder, title, message);

        final ListView list = new ListView(builder.getContext());
        if (type == ChoicePrompt.Type.MULTIPLE) {
            list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        }

        final ArrayAdapter<ModifiableChoice> adapter =
                new ArrayAdapter<ModifiableChoice>(
                        builder.getContext(), android.R.layout.simple_list_item_1) {
                    private static final int TYPE_MENU_ITEM = 0;
                    private static final int TYPE_MENU_CHECK = 1;
                    private static final int TYPE_SEPARATOR = 2;
                    private static final int TYPE_GROUP = 3;
                    private static final int TYPE_SINGLE = 4;
                    private static final int TYPE_MULTIPLE = 5;
                    private static final int TYPE_COUNT = 6;

                    private LayoutInflater mInflater;
                    private View mSeparator;

                    @Override
                    public int getViewTypeCount() {
                        return TYPE_COUNT;
                    }

                    @Override
                    public int getItemViewType(final int position) {
                        final ModifiableChoice item = getItem(position);
                        if (item.choice.separator) {
                            return TYPE_SEPARATOR;
                        } else if (type == ChoicePrompt.Type.MENU) {
                            return item.modifiableSelected ? TYPE_MENU_CHECK : TYPE_MENU_ITEM;
                        } else if (item.choice.items != null) {
                            return TYPE_GROUP;
                        } else if (type == ChoicePrompt.Type.SINGLE) {
                            return TYPE_SINGLE;
                        } else if (type == ChoicePrompt.Type.MULTIPLE) {
                            return TYPE_MULTIPLE;
                        } else {
                            throw new UnsupportedOperationException();
                        }
                    }

                    @Override
                    public boolean isEnabled(final int position) {
                        final ModifiableChoice item = getItem(position);
                        return !item.choice.separator
                                && !item.choice.disabled
                                && ((type != ChoicePrompt.Type.SINGLE && type != ChoicePrompt.Type.MULTIPLE)
                                || item.choice.items == null);
                    }

                    @Override
                    public View getView(final int position, View view, final ViewGroup parent) {
                        final int itemType = getItemViewType(position);
                        final int layoutId;
                        if (itemType == TYPE_SEPARATOR) {
                            if (mSeparator == null) {
                                mSeparator = new View(getContext());
                                mSeparator.setLayoutParams(
                                        new ListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2, itemType));
                                final TypedArray attr =
                                        getContext().obtainStyledAttributes(new int[] {android.R.attr.listDivider});
                                mSeparator.setBackgroundResource(attr.getResourceId(0, 0));
                                attr.recycle();
                            }
                            return mSeparator;
                        } else if (itemType == TYPE_MENU_ITEM) {
                            layoutId = android.R.layout.simple_list_item_1;
                        } else if (itemType == TYPE_MENU_CHECK) {
                            layoutId = android.R.layout.simple_list_item_checked;
                        } else if (itemType == TYPE_GROUP) {
                            layoutId = android.R.layout.preference_category;
                        } else if (itemType == TYPE_SINGLE) {
                            layoutId = android.R.layout.simple_list_item_single_choice;
                        } else if (itemType == TYPE_MULTIPLE) {
                            layoutId = android.R.layout.simple_list_item_multiple_choice;
                        } else {
                            throw new UnsupportedOperationException();
                        }

                        if (view == null) {
                            if (mInflater == null) {
                                mInflater = LayoutInflater.from(builder.getContext());
                            }
                            view = mInflater.inflate(layoutId, parent, false);
                        }

                        final ModifiableChoice item = getItem(position);
                        final TextView text = (TextView) view;
                        text.setEnabled(!item.choice.disabled);
                        text.setText(item.modifiableLabel);
                        if (view instanceof CheckedTextView) {
                            final boolean selected = item.modifiableSelected;
                            if (itemType == TYPE_MULTIPLE) {
                                list.setItemChecked(position, selected);
                            } else {
                                ((CheckedTextView) view).setChecked(selected);
                            }
                        }
                        return view;
                    }
                };
        addChoiceItems(type, adapter, choices, /* indent */ null);

        list.setAdapter(adapter);
        builder.setView(list);

        final AlertDialog dialog;
        if (type == ChoicePrompt.Type.SINGLE || type == ChoicePrompt.Type.MENU) {
            dialog = createStandardDialog(builder, prompt, res);
            list.setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(
                                final AdapterView<?> parent, final View v, final int position, final long id) {
                            final ModifiableChoice item = adapter.getItem(position);
                            if (type == ChoicePrompt.Type.MENU) {
                                final ChoicePrompt.Choice[] children = item.choice.items;
                                if (children != null) {
                                    // Show sub-menu.
                                    dialog.setOnDismissListener(null);
                                    dialog.dismiss();
                                    onChoicePromptImpl(
                                            session, item.modifiableLabel, /* msg */ null, type, children, prompt, res);
                                    return;
                                }
                            }
                            res.complete(prompt.confirm(item.choice));
                            dialog.dismiss();
                        }
                    });
        } else if (type == ChoicePrompt.Type.MULTIPLE) {
            list.setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(
                                final AdapterView<?> parent, final View v, final int position, final long id) {
                            final ModifiableChoice item = adapter.getItem(position);
                            item.modifiableSelected = ((CheckedTextView) v).isChecked();
                        }
                    });
            builder
                    .setNegativeButton(android.R.string.cancel, /* listener */ null)
                    .setPositiveButton(
                            android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(final DialogInterface dialog, final int which) {
                                    final int len = adapter.getCount();
                                    ArrayList<String> items = new ArrayList<>(len);
                                    for (int i = 0; i < len; i++) {
                                        final ModifiableChoice item = adapter.getItem(i);
                                        if (item.modifiableSelected) {
                                            items.add(item.choice.id);
                                        }
                                    }
                                    res.complete(prompt.confirm(items.toArray(new String[items.size()])));
                                }
                            });
            dialog = createStandardDialog(builder, prompt, res);
        } else {
            throw new UnsupportedOperationException();
        }
        dialog.show();

        prompt.setDelegate(
                new PromptInstanceDelegate() {
                    @Override
                    public void onPromptDismiss(final BasePrompt prompt) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onPromptUpdate(final BasePrompt prompt) {
                        dialog.setOnDismissListener(null);
                        dialog.dismiss();
                        final ChoicePrompt newPrompt = (ChoicePrompt) prompt;
                        onChoicePromptImpl(
                                session,
                                newPrompt.title,
                                newPrompt.message,
                                newPrompt.type,
                                newPrompt.choices,
                                newPrompt,
                                res);
                    }
                });
    }


    private LinearLayout addStandardLayout(
            final AlertDialog.Builder builder, final String title, final String msg) {
        final ScrollView scrollView = new ScrollView(builder.getContext());
        final LinearLayout container = new LinearLayout(builder.getContext());
        final int horizontalPadding = 10;
        final int verticalPadding = (msg == null || msg.isEmpty()) ? horizontalPadding : 0;
        container.setOrientation(LinearLayout.VERTICAL);
        container.setPadding(
                /* left */ horizontalPadding, /* top */ verticalPadding,
                /* right */ horizontalPadding, /* bottom */ verticalPadding);
        scrollView.addView(container);
        builder.setTitle(title).setMessage(msg).setView(scrollView);
        return container;
    }

    private AlertDialog createStandardDialog(
            final AlertDialog.Builder builder,
            final BasePrompt prompt,
            final GeckoResult<PromptResponse> response) {
        final AlertDialog dialog = builder.create();
        dialog.setOnDismissListener(
                new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(final DialogInterface dialog) {
                        if (!prompt.isComplete()) {
                            response.complete(prompt.dismiss());
                        }
                    }
                });
        return dialog;
    }

    private void addChoiceItems(
            final int type,
            final ArrayAdapter<ModifiableChoice> list,
            final ChoicePrompt.Choice[] items,
            final String indent) {
        if (type == ChoicePrompt.Type.MENU) {
            for (final ChoicePrompt.Choice item : items) {
                list.add(new ModifiableChoice(item));
            }
            return;
        }

        for (final ChoicePrompt.Choice item : items) {
            final ModifiableChoice modItem = new ModifiableChoice(item);

            final ChoicePrompt.Choice[] children = item.items;

            if (indent != null && children == null) {
                modItem.modifiableLabel = indent + modItem.modifiableLabel;
            }
            list.add(modItem);

            if (children != null) {
                final String newIndent;
                if (type == ChoicePrompt.Type.SINGLE || type == ChoicePrompt.Type.MULTIPLE) {
                    newIndent = (indent != null) ? indent + '\t' : "\t";
                } else {
                    newIndent = null;
                }
                addChoiceItems(type, list, children, newIndent);
            }
        }
    }

    private static class ModifiableChoice {
        public boolean modifiableSelected;
        public String modifiableLabel;
        public final ChoicePrompt.Choice choice;

        public ModifiableChoice(ChoicePrompt.Choice c) {
            choice = c;
            modifiableSelected = choice.selected;
            modifiableLabel = choice.label;
        }
    }
}
