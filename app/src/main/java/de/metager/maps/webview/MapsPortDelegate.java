package de.metager.maps.webview;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;
import org.mozilla.geckoview.WebExtension;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicReference;

import de.metager.maps.MainActivity;
import de.metager.maps.util.androidUtil.Geolocation;
import de.metager.maps.util.androidUtil.MGLocationListener;
import de.metager.maps.permissions.PermissionManager;

public class MapsPortDelegate implements WebExtension.PortDelegate {
    public static String GEOLOCATION_RESPONSE_TYPE_SINGLEUPDATE = "geolocation_current_position";
    public static String GEOLOCATION_RESPONSE_TYPE_WATCH = "geolocation_watch_position";
    private final LocationManager locationManager;
    private final MainActivity activity;

    private ArrayList<Geolocation> currentPositionRequests = new ArrayList<>();
    private ArrayList<Geolocation> currentWatchPositionRequests = new ArrayList<>();

    public MapsPortDelegate(MainActivity mainActivity){
        this.activity = mainActivity;
        this.locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
    }

    public void androidAppConnected(){
        activity.androidAppConnected();
    }

    public void onPortMessage(final @NonNull Object message, final @NonNull WebExtension.Port port){
        JSONObject json_message = (JSONObject)message;
        try {
            if(json_message.getString("from").equals("android")) return;
            if(json_message.getString("type").equals("geolocation_getcurrentposition")){
                currentPositionRequests.add(new Geolocation(locationManager, json_message.getJSONObject("data"), port));
                this.getCurrentPositions();
            }else if(json_message.getString("type").equals("geolocation_watchposition")){
                currentWatchPositionRequests.add(new Geolocation(locationManager, json_message.getJSONObject("data"), port));
                this.watchPositions();
            }else if(json_message.getString("type").equals("geolocation_clearwatch")) {
                int request_id = json_message.getInt("data");
                if(!this.clearWatch(request_id)){
                    Log.e("MetaGerMaps", "Failed to remove geolocation watch with id " + request_id);
                }
            }else if(json_message.getString("type").equals("display_timeout_toggle")){
                // Toggle Display Timeout i.e. when navigation is running
                boolean enableDisplayTimeout = json_message.getBoolean("data");
                if(enableDisplayTimeout) {
                    activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }else{
                    activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            }else if(json_message.getString("type").equals("message")){
                JSONObject answer = new JSONObject();
                answer.put("from", "android");
                answer.put("type", "message");
                answer.put("data", "Android App connected");
                port.postMessage(answer);
            }else if(json_message.getString("type").equals("permission_granted_geolocation")){
                JSONObject answer = new JSONObject();
                answer.put("from", "android");
                answer.put("type", "permission_granted_geolocation");
                JSONObject data = new JSONObject();
                data.put("permission_granted", PermissionManager.hasLocationPermission(this.activity));
                answer.put("data", data);
                port.postMessage(answer);
            }
        } catch (JSONException ignored) {
        }
    }

    /**
     * Will go through all pending permission requests
     * and execute them
     */
    public void watchPositions(){
        if(currentWatchPositionRequests.size() == 0) return;

        if(!PermissionManager.checkLocationPermission(activity, PermissionManager.WATCH_LOCATION_REQUEST_JAVASCRIPT)){
            return;
        }

        for(Geolocation geolocation : currentWatchPositionRequests) {
            if(geolocation.locationListener != null) continue;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                geolocation.watchPosition(activity.getMainExecutor(), location -> {
                    // Provide response object for
                    try {
                        int request_id = geolocation.requestID;
                        JSONObject location_response = this.getLocationSuccessResponse(request_id, MapsPortDelegate.GEOLOCATION_RESPONSE_TYPE_WATCH, location);
                        geolocation.port.postMessage(location_response);
                    } catch (JSONException e) {
                        Log.e("MetaGerMaps", "Cannot create Location success response");
                    }
                });
            }else{
                MapsPortDelegate selfReference = this;
                geolocation.watchPosition(activity.getMainLooper(), new MGLocationListener() {
                    @Override
                    public void onLocationChanged(@Nullable Location location) {
                        try {
                            int request_id = geolocation.requestID;
                            if(location == null){
                                selfReference.clearWatch(request_id);
                            }
                            JSONObject location_response = MapsPortDelegate.getLocationSuccessResponse(request_id, MapsPortDelegate.GEOLOCATION_RESPONSE_TYPE_WATCH, location);
                            geolocation.port.postMessage(location_response);
                        } catch (JSONException e) {
                            Log.e("MetaGerMaps", "Cannot create Location success response");
                        }
                    }
                    @Override
                    public void onProviderEnabled(@NonNull String provider) {}

                    @Override
                    public void onProviderDisabled(@NonNull String provider) {}

                    @Override
                    public void onStatusChanged(String provider, int status, Bundle extras) {}
                });
            }
        }
    }

    public void watchPositionsError(){
        for(Geolocation geolocation : currentWatchPositionRequests) {
            // Send an error back to the app
            try {
                JSONObject error_response = this.getLocationErrorResponse(1, geolocation.requestID, MapsPortDelegate.GEOLOCATION_RESPONSE_TYPE_WATCH);
                geolocation.port.postMessage(error_response);
            } catch (JSONException e) {
                Log.e("MetaGerMaps", "Error reporting back geolocation error to user");
            }
        }
        while(currentWatchPositionRequests.size() > 0){
            this.clearWatch(currentPositionRequests.get(0).requestID);
        }
    }

    private boolean clearWatch(int request_id) {
        for(Geolocation geolocation : this.currentWatchPositionRequests){
            if(geolocation.requestID == request_id){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    geolocation.clearWatch();
                    this.currentWatchPositionRequests.remove(geolocation);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Will go through all pending permission requests
     * and execute them
     */
    public void getCurrentPositions(){
        if(currentPositionRequests.size() == 0) return;

        if(!PermissionManager.checkLocationPermission(activity, PermissionManager.SINGLE_LOCATION_REQUEST_JAVASCRIPT)){
            return;
        }

        for(Geolocation geolocation : currentPositionRequests) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                AtomicReference<Geolocation> geolocation_reference = new AtomicReference<>(geolocation);
                geolocation.getCurrentPosition(activity.getMainExecutor(), location -> {
                    // Provide response object for
                    try {
                        int request_id = geolocation.requestID;

                        JSONObject location_response = MapsPortDelegate.getLocationSuccessResponse(request_id, MapsPortDelegate.GEOLOCATION_RESPONSE_TYPE_SINGLEUPDATE, location);
                        geolocation.port.postMessage(location_response);
                    } catch (JSONException e) {
                        Log.e("MetaGerMaps", "Cannot create Location success response");
                    }
                });
            }else{
                // API Level below 31. We need to use other functions
                geolocation.getCurrentPosition(activity.getMainLooper(), location -> {
                    // Provide response object for
                    try {
                        int request_id = geolocation.requestID;
                        JSONObject location_response = MapsPortDelegate.getLocationSuccessResponse(request_id, MapsPortDelegate.GEOLOCATION_RESPONSE_TYPE_SINGLEUPDATE, location);
                        geolocation.port.postMessage(location_response);
                    } catch (JSONException e) {
                        Log.e("MetaGerMaps", "Cannot create Location success response");
                    }
                });
            }
        }
        currentPositionRequests = new ArrayList<>();
    }

    /**
     *  Called when user denies location request
     *  Reports an error back to the app so it can handle
     *  the geolocation failure
     * <p>
     *  Currently only called when user denies Geolocation access
      */
    public void getCurrentPositionsError(){
        for(Geolocation geolocation : currentPositionRequests) {
            // Send an error back to the app
            try {
                JSONObject error_response = this.getLocationErrorResponse(1, geolocation.requestID, MapsPortDelegate.GEOLOCATION_RESPONSE_TYPE_SINGLEUPDATE);
                geolocation.port.postMessage(error_response);
            } catch (JSONException e) {
                Log.e("MetaGerMaps", "Error reporting back geolocation error to user");
            }

        }
    }

    public static JSONObject getLocationErrorResponse(int code, int request_id, String response_type) throws JSONException {
        JSONObject response = new JSONObject();

        response.put("from", "android");
        response.put("type", response_type);

        JSONObject data = new JSONObject();
        data.put("request_id", request_id);
        data.put("type", "error");

        JSONObject error = new JSONObject();
        switch(code){
            case 1:
                error.put("code", 1);
                error.put("message", "User denied geolocation prompt");
                break;
            case 2:
                error.put("code", 2);
                error.put("message", "Geolocation unavailable");
                break;
            case 3:
                error.put("code", 3);
                error.put("message", "Geolocation request timeout");
                break;
        }
        data.put("error", error);

        response.put("data", data);

        return response;
    }

    public static JSONObject getLocationSuccessResponse(int request_id, String response_type, Location location) throws JSONException {
        if(location == null){
            return MapsPortDelegate.getLocationErrorResponse(3, request_id, response_type);
        }
        JSONObject response = new JSONObject();

        response.put("from", "android");
        response.put("type", response_type);

        JSONObject data = new JSONObject();
        data.put("request_id", request_id);
        data.put("type", "location");

        JSONObject location_object = new JSONObject();
        location_object.put("timestamp", location.getTime());
        JSONObject coords = new JSONObject();
        coords.put("latitude", location.getLatitude());
        coords.put("longitude", location.getLongitude());
        coords.put("altitude", location.getAltitude());
        coords.put("accuracy", location.getAccuracy());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            coords.put("altitudeAccuracy", location.getVerticalAccuracyMeters());
        }else{
            coords.put("altitudeAccuracy", "null");
        }
        if(location.hasBearing()) {
            if(location.hasSpeed() && location.getSpeed() > 0) {
                coords.put("heading", location.getBearing());
            }else{
                coords.put("heading", "NaN");
            }
        }else{
            coords.put("heading", "null");
        }
        if(location.hasSpeed()){
            coords.put("speed", location.getSpeed());
        }else{
            coords.put("speed", "null");
        }
        location_object.put("coords", coords);
        data.put("location", location_object);

        response.put("data", data);

        return response;
    }

    public void onDisconnect(final @NonNull WebExtension.Port port) {}
}
