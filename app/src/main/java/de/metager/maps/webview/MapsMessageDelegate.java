package de.metager.maps.webview;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;
import org.mozilla.geckoview.GeckoResult;
import org.mozilla.geckoview.WebExtension;

public class MapsMessageDelegate implements WebExtension.MessageDelegate {
    private final MapsPortDelegate mPortDelegate;
    private WebExtension.Port mPort;

    public MapsMessageDelegate(MapsPortDelegate portDelegate) {
        this.mPortDelegate = portDelegate;
    }

    public void onConnect(final @NonNull WebExtension.Port port){
        this.mPort = port;
        mPort.setDelegate(mPortDelegate);

        // Post message about connected Android app when port connects
        JSONObject message = new JSONObject();
        try {
            message.put("from", "android");
            message.put("type", "message");
            message.put("data", "Android App connected");
            mPort.postMessage(message);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        mPortDelegate.androidAppConnected();
    }

    public void updateInsets(int top, int right, int bottom, int left){
        // Post message about connected Android app when port connects
        JSONObject message = new JSONObject();
        try {
            message.put("from", "android");
            message.put("type", "insets");

            JSONObject data = new JSONObject();
            data.put("top", top);
            data.put("right", right);
            data.put("bottom", bottom);
            data.put("left", left);
            message.put("data", data);
            mPort.postMessage(message);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public GeckoResult<Object> onMessage(final @NonNull String nativeApp, final @NonNull Object message, final @NonNull WebExtension.MessageSender sender){
        return null;
    }
}
