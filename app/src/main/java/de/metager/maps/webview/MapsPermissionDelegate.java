package de.metager.maps.webview;

import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import org.mozilla.geckoview.GeckoResult;
import org.mozilla.geckoview.GeckoSession;

import de.metager.maps.MainActivity;

public class MapsPermissionDelegate implements GeckoSession.PermissionDelegate {

    private final AppCompatActivity activity;
    private Callback callback;

    public MapsPermissionDelegate(AppCompatActivity activity){
        this.activity = activity;
    }

    @Override
    public GeckoResult<Integer> onContentPermissionRequest(
            @NonNull final GeckoSession session, @NonNull ContentPermission perm) {
        return GeckoResult.fromValue(ContentPermission.VALUE_ALLOW);
    }
    @Override
    public void onAndroidPermissionsRequest(final GeckoSession session,
                                            final String[] permissions,
                                            final Callback callback) {
        this.callback = callback;
        this.activity.requestPermissions(permissions, MainActivity.LOCATION_PERMISSION_REQUEST_CODE);

    }

    public void onRequestPermissionsResult(int i, String[] strings, int[] ints) {
        for (final int result : ints) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                // At least one permission was not granted.
                callback.reject();
                this.callback = null;
                return;
            }
        }
        this.callback.grant();
        this.callback = null;
    }

}
