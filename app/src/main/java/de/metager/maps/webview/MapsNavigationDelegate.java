package de.metager.maps.webview;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.browser.customtabs.CustomTabColorSchemeParams;
import androidx.browser.customtabs.CustomTabsClient;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.content.ContextCompat;

import org.mozilla.geckoview.AllowOrDeny;
import org.mozilla.geckoview.GeckoResult;
import org.mozilla.geckoview.GeckoSession;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import de.metager.maps.R;

public class MapsNavigationDelegate implements GeckoSession.NavigationDelegate {
    private final ArrayList<URL> allowedHosts;
    private final Context mContext;
    public boolean canGoBack = false;

    public MapsNavigationDelegate(ArrayList<URL> allowedHosts, Context context) {
        super();
        this.allowedHosts = allowedHosts;
        this.mContext = context;
    }

    @Override
    public void onCanGoBack(@NonNull final GeckoSession session, final boolean canGoBack) {
        this.canGoBack = canGoBack;
    }

    @Override
    public GeckoResult<AllowOrDeny> onLoadRequest(
            final GeckoSession session, final LoadRequest request) {
        // We'll only allow loading URLs that are on this Apps domain.
        // Other URLs we'll open in a system browser
        // Determine this apps URL
        Uri uri = Uri.parse(request.uri);
        for (URL allowed_url: allowedHosts) {
            if(uri.getScheme().startsWith("http") && uri.getHost().contains(allowed_url.getHost())){
                return GeckoResult.allow();
            }
        }
        this.requestURLOpen(uri);
        return GeckoResult.deny();
    }

    private void requestURLOpen(Uri uri) {
        // Check if it is a URL to be opened
        if(uri.getScheme().startsWith("http")){
            // Try to open in custom tabs
            if(CustomTabsClient.getPackageName(mContext, Collections.emptyList()) == null){
                // Custom tabs not available. Just try to open in system browser
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                if(i.resolveActivity(mContext.getPackageManager()) != null){
                    mContext.startActivity(i);
                }else{
                    // Maybe show the user a message in this case?
                    Log.e("NavigationDelegate", "Cannot request URL to be opened in external browser");
                }
            }else{
                // Custom Tabs are available
                CustomTabsIntent i = new CustomTabsIntent.Builder()
                        .setDefaultColorSchemeParams(
                                new CustomTabColorSchemeParams.Builder()
                                        .setToolbarColor(ContextCompat.getColor(mContext, R.color.colorPrimary))
                                        .build())
                        .setColorSchemeParams(CustomTabsIntent.COLOR_SCHEME_DARK,
                                new CustomTabColorSchemeParams.Builder()
                                        .setToolbarColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark))
                                        .build())
                        .build();
                i.launchUrl(mContext, uri);
            }
        }else if(uri.getScheme().startsWith("tel")){
            // Telephone number
            Intent i = new Intent(Intent.ACTION_DIAL, uri);
            if(i.resolveActivity(mContext.getPackageManager()) != null){
                mContext.startActivity(i);
            }else{
                Log.e("NavigationDelegate", "Cannot request Telephone number to be opened in dial intent");
            }
        }else if(uri.getScheme().startsWith("mailto")){
            // Telephone number
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            if(i.resolveActivity(mContext.getPackageManager()) != null){
                mContext.startActivity(i);
            }else{
                Log.e("NavigationDelegate", "Cannot request email address to be opened in sendto intent");
            }
        }
    }
}
