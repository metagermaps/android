package de.metager.maps.webview;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import de.metager.maps.MainActivity;
import de.metager.maps.util.androidUtil.ConnectivityUtil;

public class MapsJavascriptInterface {
    private final Context context;

    public MapsJavascriptInterface(Context context) {
        this.context = context;
    }

    @android.webkit.JavascriptInterface
    public boolean isWireless() {
        return ConnectivityUtil.isWireless(context);
    }

    @android.webkit.JavascriptInterface
    public boolean isInternetAvailable() {
        return ConnectivityUtil.internetIsAvailable(context);
    }

    @JavascriptInterface
    public long getVersionCode() {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                return pInfo.getLongVersionCode();
            }else{
                return (long)pInfo.versionCode;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @android.webkit.JavascriptInterface
    public void toggleDisplayTimeout(final boolean enabled) {
        MainActivity activity = (MainActivity)context;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!enabled) {
                    ((MainActivity) context).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    Toast.makeText(context, "Display Timeout abgeschaltet", Toast.LENGTH_LONG).show();
                }else{
                    ((MainActivity) context).getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    Toast.makeText(context, "Display Timeout angeschaltet", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
