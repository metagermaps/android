package de.metager.maps.webview;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class InsetChangeController {
    private final View mView;
    private final MapsMessageDelegate mMessageDelegate;
    private final float displayDensity;

    public InsetChangeController(View view, float displayDensity, MapsMessageDelegate messageDelegate) {
        this.mView = view;
        this.mMessageDelegate = messageDelegate;
        this.displayDensity = displayDensity;

        WindowInsetsCompat current_insets = ViewCompat.getRootWindowInsets(mView);
        if(current_insets != null){
            Insets new_insets = current_insets.getInsets(WindowInsetsCompat.Type.systemGestures());
            this.insetsChanged(new_insets);
        }

        ViewCompat.setOnApplyWindowInsetsListener(mView, (v, windowInsets) -> {
            Insets gestures = windowInsets.getInsets(WindowInsetsCompat.Type.systemGestures());

            insetsChanged(gestures);
            return WindowInsetsCompat.CONSUMED;
        });
    }

    private void insetsChanged(@NonNull Insets insets){
        int top = (int)Math.round(insets.top / displayDensity);
        int right = (int)Math.round(insets.right / displayDensity);
        int bottom = (int)Math.round(insets.bottom / displayDensity);
        int left = (int)Math.round(insets.left / displayDensity);

        mMessageDelegate.updateInsets(top, right, bottom, left);
    }
}
