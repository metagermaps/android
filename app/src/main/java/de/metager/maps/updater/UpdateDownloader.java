package de.metager.maps.updater;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.common.util.concurrent.ListenableFuture;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class UpdateDownloader extends Worker {
    public static final String TAG = "UPDATER";
    public static final String PROGRESS_MAX = "PROGRESS_MAX";
    public static final String PROGRESS_CURRENT = "PROGRESS_CURRENT";
    public static final String APK_URL = "APK_URL";
    public static final String LOCAL_APK_URI = "LOCAL_APK_URI";
    private final String downloadUrl;
    private final File destinationUpdateFile;

    public UpdateDownloader(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        setProgressAsync(new Data.Builder().putInt(PROGRESS_CURRENT, 0).build());
        this.downloadUrl = getInputData().getString(APK_URL);
        this.destinationUpdateFile = new File(getInputData().getString(LOCAL_APK_URI));
    }

    @NonNull
    @Override
    public Result doWork() {
        if(destinationUpdateFile.exists()){
            destinationUpdateFile.delete();
        }
        URL downloadUrl = null;
        try {
            downloadUrl = new URL(this.downloadUrl);
            HttpsURLConnection connection = (HttpsURLConnection) downloadUrl.openConnection();
            int downloadedBytes = 0;
            int totalBytes = connection.getContentLength();
            int currentProgress = 0;
            if (totalBytes != -1) {
                setProgressAsync(new Data.Builder().putInt(PROGRESS_MAX, connection.getContentLength()).build()).get();
            }
            if (connection.getResponseCode() != HttpsURLConnection.HTTP_OK) {
                return Result.failure();
            }
            try (InputStream is = connection.getInputStream(); FileOutputStream os = new FileOutputStream(destinationUpdateFile)) {
                int bytesRead = -1;
                byte[] buffer = new byte[512];
                while ((bytesRead = is.read(buffer)) != -1) {
                    os.write(buffer, 0, bytesRead);
                    downloadedBytes += bytesRead;
                    int newProgress = (int) Math.round(downloadedBytes * 100.0 / totalBytes);
                    if(newProgress > currentProgress) {
                        currentProgress = newProgress;
                        setProgressAsync(new Data.Builder()
                                .putInt(PROGRESS_MAX, totalBytes)
                                .putInt(PROGRESS_CURRENT, downloadedBytes).build()).get();
                    }
                }
            }
            return Result.success();
        } catch (Exception e) {
            return Result.failure();
        }
    }
}
