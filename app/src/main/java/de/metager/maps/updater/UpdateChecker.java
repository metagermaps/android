package de.metager.maps.updater;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import de.metager.maps.BuildConfig;
import de.metager.maps.notifications.NotificationManager;

public class UpdateChecker extends Worker {
    public static final String NAME = "UPDATE_CHECKER";
    public static final String PERIODIC_TAG = "UPDATE_CHECKER_PERIODIC";
    private final static String RELEASES_API_ENDPOINT = "https://gitlab.metager.de/api/v4/projects/60/releases?per_page=1";
    private final static int RELEASES_TO_CONSIDER = 10; // How many of the newest releases to compare the current version against
    private final Map<String, String> supported_abis = new HashMap<String, String>() {{
        put("arm64-v8a", "ARM (64 bit)");
        put("armeabi-v7a", "ARM (32 bit)");
        put("x86_64", "x86 (64 bit)");
        put("x86", "x86 (32 bit)");
    }};
    private @NonNull Context mAppContext;

    public UpdateChecker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        if (context == null) {
            throw new IllegalArgumentException("Application Context is null");
        }
        this.mAppContext = context;
    }

    @NonNull
    @Override
    @RequiresPermission("android.permission.POST_NOTIFICATIONS")
    public Result doWork() {
        try {
            String apkURL = this.updateAvailable();
            if (apkURL == null) {
                return Result.success();
            }
            NotificationManager.createUpdateNotification(mAppContext, apkURL);

            return Result.success();
        } catch (Exception e) {
            Log.e("MetaGerMaps Updater", e.toString());
            return Result.failure();
        }
    }

    private String updateAvailable() throws IOException, JSONException {
        String currentVersion = BuildConfig.VERSION_NAME;

        URL url = new URL(UpdateChecker.RELEASES_API_ENDPOINT);
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.setRequestProperty("User-Agent", "MetaGer Maps Android (" + currentVersion + ")");
        int responseCode = con.getResponseCode();
        if (responseCode == HttpsURLConnection.HTTP_OK) {
            StringBuilder responseBuilder = new StringBuilder();
            try (Reader reader = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8))) {
                int c = 0;
                while ((c = reader.read()) != -1) {
                    responseBuilder.append((char) c);
                }
            }
            JSONArray releases = new JSONArray(responseBuilder.toString());
            JSONObject release = releases.getJSONObject(0);
            String releaseName = release.getString("name");

            if (!currentVersion.startsWith(releaseName)) {
                // Update Available
                JSONArray links = release.getJSONObject("assets").getJSONArray("links");
                String releaseLinkVersion = "Universal APK";
                for(String abi : Build.SUPPORTED_ABIS){
                    if(supported_abis.containsKey(abi)){
                        releaseLinkVersion = supported_abis.get(abi);
                        break;
                    }
                }
                for (int i = 0; i < links.length(); i++) {
                    JSONObject link = links.getJSONObject(i);
                    String name = link.getString("name");
                    if (name.equals(releaseLinkVersion)) {
                        return link.getString("direct_asset_url");
                    }
                }
            }
        }
        return null;
    }
}
