package de.metager.maps.updater;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.core.content.PackageManagerCompat;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import java.io.File;
import java.io.IOException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.concurrent.ExecutionException;

import de.metager.maps.BuildConfig;
import de.metager.maps.R;
import de.metager.maps.databinding.ActivityUpdateBinding;

public class UpdateActivity extends AppCompatActivity {

    private String downloadUrl;
    private File destinationUpdateFile;
    private long downloadId;
    private ProgressBar downloadProgressBar;
    private TextView downloadProgressText;
    private Button startInstallationButton;

    public UpdateActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        de.metager.maps.databinding.ActivityUpdateBinding binding = ActivityUpdateBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.toolbar.setTitle(getString(R.string.notification_update_title));

        setSupportActionBar(binding.toolbar);

        this.downloadProgressBar = (ProgressBar) this.findViewById(R.id.progressBar);
        downloadProgressBar.setVisibility(View.GONE);
        this.downloadProgressText = (TextView) this.findViewById(R.id.progressText);
        downloadProgressText.setVisibility(View.GONE);
        this.startInstallationButton = (Button) this.findViewById(R.id.installButton);
        this.downloadUrl = getIntent().getExtras().getString("downloadUrl");
        if (this.validateDownloadUrl()) {
            startInstallationButton.setOnClickListener(v -> {
                try {
                    this.startInstallation();
                } catch (IOException | ExecutionException | InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }

    @Override
    public void onPause() {
        WorkManager.getInstance(this).cancelAllWorkByTag(UpdateDownloader.TAG);
        super.onPause();
    }

    @Override
    public void onResume(){
        downloadProgressBar.setVisibility(View.GONE);
        downloadProgressText.setVisibility(View.GONE);
        downloadProgressText.setText("");
        downloadProgressBar.setProgress(0);
        startInstallationButton.setEnabled(true);
        super.onResume();
    }

    @Override
    public void onDestroy() {
        WorkManager.getInstance(this).cancelAllWorkByTag(UpdateDownloader.TAG);
        super.onDestroy();
    }

    private void startInstallation() throws IOException, ExecutionException, InterruptedException {
        // Check if we are allowed to install apps from unknown source
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && !getPackageManager().canRequestPackageInstalls()) {
            startActivity(new Intent(android.provider.Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES, Uri.parse("package:" + getPackageName())));
            return;
        }
        // Download APK
        downloadProgressBar.setVisibility(View.VISIBLE);
        downloadProgressText.setVisibility(View.VISIBLE);
        downloadProgressBar.setIndeterminate(false);
        downloadProgressBar.setProgress(0);
        startInstallationButton.setEnabled(false);


        String fileName = "de.metager.maps-app_update.apk";

        // Build Uri for APK file in Downloads folder
        destinationUpdateFile = new File(getExternalCacheDir(), fileName);

        //destinationUpdateFile = new File("file://" + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + fileName);
        if (destinationUpdateFile.exists()) {
            destinationUpdateFile.delete();
        }


        OneTimeWorkRequest downloadRequest = new OneTimeWorkRequest.Builder(UpdateDownloader.class)
                .addTag(UpdateDownloader.TAG)
                .setInputData(new Data.Builder()
                        .putString(UpdateDownloader.APK_URL, downloadUrl)
                        .putString(UpdateDownloader.LOCAL_APK_URI, destinationUpdateFile.getAbsolutePath()).build()).build();
        WorkManager.getInstance(this).enqueue(downloadRequest);
        WorkManager.getInstance(this).getWorkInfoByIdLiveData(downloadRequest.getId()).observe(this, workInfo -> {
            if(workInfo == null) return;
            if(workInfo.getState().isFinished()){
                if(workInfo.getState() == WorkInfo.State.SUCCEEDED){
                    installOnFinish();
                }else{
                    finish();
                }
            }else{
                Data progress = workInfo.getProgress();
                int downloadedBytes = progress.getInt(UpdateDownloader.PROGRESS_CURRENT, 0);
                int maxBytes = progress.getInt(UpdateDownloader.PROGRESS_MAX, Integer.MAX_VALUE);
                downloadProgressBar.setMax(maxBytes);
                downloadProgressBar.setProgress(downloadedBytes);
                downloadProgressText.setText(humanReadableByteCountBin(downloadedBytes) + " / " + humanReadableByteCountBin(maxBytes));
            }
        });
    }

    private static String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " B";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %ciB", value / 1024.0, ci.current());
    }

    private void installOnFinish() {
        Uri apkFileUri;
        Intent installIntent;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            apkFileUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", destinationUpdateFile);
            installIntent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
            installIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            installIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            installIntent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
        }else{
            apkFileUri = Uri.fromFile(destinationUpdateFile);
            installIntent = new Intent(Intent.ACTION_VIEW);
        }
        installIntent.setDataAndType(apkFileUri, "application/vnd.android.package-archive");
        startActivity(installIntent);

    }

    private boolean validateDownloadUrl() {
        if (this.downloadUrl == null || !this.downloadUrl.startsWith("https://gitlab.metager.de/metagermaps/android")) {
            return false;
        }
        return true;
    }
}