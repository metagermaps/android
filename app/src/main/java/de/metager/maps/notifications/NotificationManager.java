package de.metager.maps.notifications;

import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresPermission;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import de.metager.maps.R;
import de.metager.maps.updater.UpdateActivity;

public class NotificationManager {
    public static final String NOTIFICATION_UPDATER_CHANNEL_ID = "UPDATER";
    public static final int NOTIFICATION_UPDATER_ID = 0;

    public static void createUpdateNotificationChannel(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.notification_channel_updater_name);
            String description = context.getString(R.string.notifictaion_channel_udpater_description);
            int importance = android.app.NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(NotificationManager.NOTIFICATION_UPDATER_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this.
            android.app.NotificationManager notificationManager = context.getSystemService(android.app.NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @RequiresPermission("android.permission.POST_NOTIFICATIONS")
    public static void createUpdateNotification(Context context, String apkUrl) {
        Intent intent = new Intent(context, UpdateActivity.class);
        intent.putExtra("downloadUrl", apkUrl);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationManager.NOTIFICATION_UPDATER_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_update)
                .setContentTitle(context.getString(R.string.notification_update_title))
                .setContentText(context.getString(R.string.notification_update_description))
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent);
        NotificationManagerCompat.from(context).notify(NotificationManager.NOTIFICATION_UPDATER_ID, builder.build());
    }
}
