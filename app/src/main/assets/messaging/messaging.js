let port = browser.runtime.connectNative("browser");

port.onMessage.addListener(message => {
    window.postMessage(message);
});

window.addEventListener("message", event => {
  if(!event.data || !event.data.from || event.data.from != "web") return;
  console.log(JSON.stringify(event.data));
  port.postMessage(event.data);
});

port.onDisconnect.addListener(p => {
    if(p.error){
        console.error(`Disconnected due to an error: ${p.error.message}`);
    }
});